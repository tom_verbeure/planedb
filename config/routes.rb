Rails.application.routes.draw do
  #get 'plane_map/index'

  get 'plane_map/:id', 					to: "plane_map#index"
  get 'plane_map/:year/:month/:day', 	to: "plane_map#index"

  get 'plane_tracks/:id', 						to: "plane_tracks#search"
  get 'plane_tracks/:year/:month/:day', 		to: "plane_tracks#search"

  get 'plane_list/:year/:month/:day', to: "plane_list#show"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'plane_list#today'
end
