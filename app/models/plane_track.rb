require 'pp' 
require 'gpx'

#LAT = 37.350304
#LON = -122.046994


AIRPORTS = {
    "SJC" => [ 
				{
					:lat =>  37.37438623,
					:lon =>  -121.94077492,
    			},
				{
					:lat =>  37.35214698,
					:lon => -121.916399
    			},
			],
    "SFO" => [ 
				{
					:lat => 37.6287798, 
					:lon => -122.39623547,
    			},
				{
					:lat => 37.61355151,
					:lon => -122.35761166,
    			},

				{
					:lat => 37.62721632,
					:lon => -122.36885548,
    			},
				{
					:lat => 37.6059362,
					:lon => -122.38293171,
    			},
			],
    "PAO" => [ 
				{
					:lat => 37.46426514, 
					:lon => -122.11811185,
    			},
				{
					:lat => 37.45791206,
					:lon => -122.11212516,
    			},
			],
    "SQL" => [ 
				{
					:lat => 37.51534266,
					:lon => -122.25343466,
    			},
				{
					:lat => 37.5092152,
					:lon => -122.24661112,
    			},
			],
    "OAK" => [ 
				{
					:lat => 37.7198803,
					:lon => -122.24156857,
    			},
				{
					:lat => 37.70134318,
					:lon => -122.21410275,
    			},

				{
					:lat => 37.73013144,
					:lon => -122.22508907,
    			},
				{
					:lat => 37.72402166,
					:lon => -122.20517635,
    			},
			],
    "HWD" => [ 
				{
					:lat => 37.66235258,
					:lon => -122.12938786,
    			},
				{
					:lat => 37.65406274,
					:lon => -122.11256504,
    			},
			],
    "NUQ" => [ 
				{
					:lat => 37.42893314,
					:lon => -122.05420017,
    			},
				{
					:lat => 37.40514193,
					:lon => -122.04278469,
    			},
			],
    "HAF" => [ 
				{
					:lat => 37.518542,
					:lon => -122.507322,
    			},
				{
					:lat => 37.508432,
					:lon => -122.495177,
    			},
			],
    "RVH" => [ 
				{
					:lat => 37.336668,
					:lon => -121.822591,
    			},
				{
					:lat => 37.329783, 
					:lon => -121.816283,
    			},
			],
}

class PlaneTrack < ApplicationRecord

	include AdsbSupport

	has_many :plane_track_points, -> { order(:time) }
	has_many :plane_track_airports

	@@factory = RGeo::Geographic.spherical_factory(:srid => 4326, :has_z_coordinate => true)

	def add_point_to_track(lat, lon, altitude, time, speed)

		logger.debug("add_point_to_track: #{lat}, #{lon}, #{altitude}, #{time.to_i}, #{speed}")

		track_point = PlaneTrackPoint.new
		track_point.time = time
		track_point.speed = speed
		track_point.coordinate = @@factory.point(lon, lat, altitude)

		self.plane_track_points << track_point
		self.save!
	end

	def get_closest_distance(lat, lon)
		query = <<EOS
			SELECT ST_Distance(subquery.line, ST_SetSRID(ST_MakePoint(#{lon}, #{lat}),4326), TRUE)/1000 as distance, St_AsText(subquery.line) as line
			FROM (
				-- Convert points of a track into a line string
				SELECT pts.plane_track_id, (ST_MakeLine( ST_SetSRID(ST_MakePoint(ST_X(ST_AsText(pts.coordinate)),ST_Y(ST_AsText(pts.coordinate)),ST_Z(ST_AsText(pts.coordinate)), 0.0),4326) ORDER BY time)) as line
					FROM plane_track_points as pts
					WHERE pts.plane_track_id = #{ self.id }
					GROUP BY pts.plane_track_id
				) AS subquery;
EOS
		
		sql_result = ActiveRecord::Base.connection.execute(query)
		return nil unless sql_result.values.size > 0

		sql_data = sql_result.first

		pp sql_data
											  
		result = sql_data["distance"]
	end

	def get_closest_point(lat, lon, max_distance)

		query = <<EOS
						SELECT ST_Distance(coordinate, ST_MakePoint(#{lon}, #{lat}))/1000 as distance_km, ST_AsText(coordinate) as coordinate, time, speed
							from plane_track_points 
							where plane_track_points.plane_track_id = #{ self.id } and ST_Distance(coordinate, ST_MakePoint(#{lon}, #{lat}))/1000 < #{max_distance}
							order by ST_Distance(coordinate, ST_MakePoint(#{lon}, #{lat}))
							limit 1
						;
EOS
		
		sql_result = ActiveRecord::Base.connection.execute(query)

		return nil unless sql_result.values.size > 0

		sql_data = sql_result.first
											  
		dist 	= sql_data["distance_km"].to_f
		speed 	= sql_data["speed"].to_i
		time 	= DateTime.parse(sql_data["time"]).to_time.in_time_zone('Pacific Time (US & Canada)')
		sql_data["coordinate"] =~ /(-?\d+\.?\d+?) (-?\d+\.?\d+?) (-?\d+\.?\d+?)/
		coord 	= @@factory.point($1, $2, $3)

		result = {
			:dist		=> dist,
			:time		=> time,
			:speed		=> speed,
			:coord		=> coord,
		}

        result
	end

	def get_closest_point_below_altitude(lat, lon, altitude, max_distance)

		query = <<EOS
						SELECT ST_Distance(coordinate, ST_MakePoint(#{lon}, #{lat}))/1000 as distance_km, ST_AsText(coordinate) as coordinate, time as time
							from plane_track_points 
							where plane_track_points.plane_track_id = #{ self.id } and ST_Z(ST_AsText(coordinate)) < #{altitude} and ST_Distance(coordinate, ST_MakePoint(#{lon}, #{lat}))/1000 < #{max_distance}
							order by ST_Distance(coordinate, ST_MakePoint(#{lon}, #{lat}))
							limit 1
						;
EOS
		
		sql_result = ActiveRecord::Base.connection.execute(query)

		return nil unless sql_result.values.size > 0
											  
		dist 	= sql_result.first["distance_km"].to_f
		time 	= Time.new(sql_result.first["time"])
		sql_result.first["coordinate"] =~ /(-?\d+\.?\d+?) (-?\d+\.?\d+?) (-?\d+\.?\d+?)/
		coord 	= @@factory.point($1, $2, $3)

		result = {
			:dist		=> dist,
			:time		=> time,
			:coord		=> coord,
		}

        result
	end

    def get_closest_point_to_airport(airport)
		results = []
		AIRPORTS[airport].each do |coord|
        	lat = coord[:lat]
        	lon = coord[:lon]
			result = get_closest_point_below_altitude(lat, lon, 500, 3)
			results << result if result
		end

		results.min{ |r| r[:dist] }
    end


	def was_at_airport(airport)
        result = get_closest_point_to_airport(airport)
		return result
	end

	def was_at_airports(airports)
		codes = []
		AIRPORTS.each_key do |airport_code|
			codes << airport_code if was_at_airport(airport_code)
		end

		codes
	end

	def is_from_airport(airport)
		first_points = self.plane_track_points.take(10)
		z = first_points.collect{ |p| p.coordinate.z }

		return z.min < 300
	end

	def is_to_airport(airport)
		last_points = self.plane_track_points.reorder("time DESC").take(10)
		z = last_points.collect{ |p| p.coordinate.z }

		return z.min < 300
	end

	def get_flightaware_data
		username = 'TomV1'
		apiKey = '30ea06d91e33987bfa4281a7dabd9e24e3f4add7'

		begin
			test = FlightXML2REST.new(username, apiKey)

			result = test.FlightInfoEx(FlightInfoExRequest.new(15, self.registration, 0))
		
			time = self.created_at

			pp result.flightInfoExResult

			result.flightInfoExResult.flights.each do |flight|
				puts "#{Time.at flight.actualdeparturetime} #{Time.at flight.actualarrivaltime} #{Time.at flight.estimatedarrivaltime} :  #{Time.at time.to_i}"
				if (time.to_i >= flight.actualdeparturetime && time.to_i <= flight.actualarrivaltime) || 
				   (time.to_i >= flight.filed_departuretime && time.to_i <= flight.estimatedarrivaltime)
					self.from_airport 	= flight.origin 	 if flight.origin.size <= 4
					self.to_airport 	= flight.destination if flight.destination.size <= 4

					airports = []
					airports << self.from_airport if self.from_airport
					airports << self.to_airport if self.to_airport

					self.airports		= airports.join(",")
					self.flight			= flight.ident
				end
			end
		rescue RuntimeError => e
			pp e
			puts e.message
		end

		self.checked_flight_aware = 1
		self.save!
	end


	def to_gpx_track
		track = GPX::Track.new
		track.name = "Flight: #{self.flight}<br/>Registration: #{self.registration}<br/>ICAO: #{self.icao}<br/>"

		seg = GPX::Segment.new

		plane_track_points.each do |pt|
			p = GPX::Point.new({ lat: pt.coordinate.lat, lon: pt.coordinate.lon, time: pt.time })
			seg.points << p
		end
		track.segments << seg

		track
	end

	def to_gpx_waypoints
		waypoints = []

		plane_track_points.each do |pt|
			time_local = pt.time.to_time.in_time_zone('Pacific Time (US & Canada)')

			w = GPX::Waypoint.new({ lat: pt.coordinate.lat, lon: pt.coordinate.lon, time: pt.time, ele: pt.coordinate.z })
			w.name = "Flight: #{self.flight}<br/>Registration: #{self.registration}<br/>ICAO: #{self.icao}<br/>Altitude: #{pt.coordinate.z}<br/>Time: #{time_local}<br/>"
			waypoints << w
		end

		waypoints
	end

	def to_gpx
		gpx = GPX::GPXFile.new

		gpx.tracks << to_gpx_track
		to_gpx_waypoints.each { |w| gpx.waypoints << w }

		gpx
	end

	def split_segments
		points = self.plane_track_points
		return if points.size == 0

		prev_time = points.first.time
		new_pt = nil

		ActiveRecord::Base.transaction do 

			points.each do |cur_p|
				time_delta = cur_p.time - prev_time
	
				if time_delta > 10.minutes
					# When we haven't seen a plane for more than 10 minutes, consider it a new plane track...
					if new_pt == nil
						self.last_pos_time = prev_time
						self.save!
					else
						new_pt.save!
					end
	
					new_pt = self.dup
					new_pt.flight = nil
					new_pt.save!
				end
	
				if new_pt
					new_pt.last_pos_time = cur_p.time
					new_pt.save!
	
					cur_p.plane_track_id = new_pt.id
					cur_p.save!
				end
	
				prev_time = cur_p.time
			end

		end
	end

	def create_track_linestring
	end


	#============================================================
	# Enter new plane track data
	#============================================================

	def self.process_single_plane(json_plane)
		vrs_id = json_plane["Id"].to_i

		logger.debug "Checking plane #{vrs_id}"

		# Find the most recent plane track for a particular vrs_id, or create a new PlaneTrack
		pt = PlaneTrack.all.where(vrs_id: vrs_id).order("last_pos_time DESC").first rescue nil

		pos_time = DateTime.strptime(json_plane["PosTime"].to_s,'%Q') if json_plane["PosTime"]

		if pt 
			if pt.last_pos_time
				delta = pos_time.to_i - pt.last_pos_time.to_i
				if delta > 10.minutes
					# We haven't seen this plane for more than 10 minutes. Create a new plane track
					pt = PlaneTrack.create(vrs_id: vrs_id)
				end
			end
		else
			pt = PlaneTrack.create(vrs_id: vrs_id)
		end

		pt.icao 			= json_plane["Icao"] 	unless pt.icao 			|| !json_plane["Icao"]
		pt.flight 			= json_plane["Call"] 	unless pt.flight 		|| !json_plane["Call"]
		pt.registration		= json_plane["Reg"]  	unless pt.registration	|| !json_plane["Reg"]
		pt.plane_type		= json_plane["Type"] 	unless pt.plane_type	|| !json_plane["Type"]
		pt.model			= json_plane["Mdl"]	 	unless pt.model			|| !json_plane["Mdl"]
		pt.manufacturer		= json_plane["Man"]	 	unless pt.manufacturer	|| !json_plane["Man"]
		pt.operator			= json_plane["Op"]   	unless pt.operator		|| !json_plane["Op"]
		pt.operator_icao	= json_plane["OpIcao"]  unless pt.operator_icao	|| !json_plane["OpIcao"]

		if pos_time

			if (!pt.last_pos_time || pos_time > pt.last_pos_time)
				logger.debug "Previous time: %s, New Time: %s" % [ pt.last_pos_time, pos_time ]

				pt.last_pos_time = pos_time

				lat = json_plane["Lat"]
				lon = json_plane["Long"]
				alt = json_plane["GAlt"]
				speed = json_plane["Spd"]

				logger.debug("#{json_plane}") 

				pt.add_point_to_track(lat, lon, alt, pos_time, speed)
			end
		end

		pt.save!
	end

	def self.process_adsb_json(json)
		json["acList"].each do |json_plane|
			self.process_single_plane(json_plane)
		end
	end

    def self.process_full_day
        Dir.glob("./raw_data/2016-06-12/*.json").each do |filename|
            puts "Processing: #{filename}..."

            file = File.new(filename, 'r')
            json = Yajl::Parser.parse(file)

            puts "#{json["acList"].size} planes."

            local_planes = 0

            json["acList"].each do |json_plane|
                lon = json_plane["Long"]
                lat = json_plane["Lat"]

                next unless lon && lon >= -122.5 && lon <= -121.5
                next unless lat && lat >=   36.3 && lat <=   37.3 

                local_planes += 1
                self.process_single_plane(json_plane)
            end

            puts "#{local_planes} local planes."
        end
    end

	#============================================================
	# Plane data gathering
	#============================================================

	def self.planes_of_the_day(time, lat, lon, distance, altitude)

		todays_planes = []
		PlaneTrack.where(last_pos_time: time..time+1.day).each do |plane|
			result = plane.get_closest_point(lat,lon,distance)
			next unless result

			dist = result[:dist]
            alt = result[:coord].z

			next if dist > distance
            next if alt > altitude

			result[:plane] = plane
			result[:airports] = plane.was_at_airports(AIRPORTS)

			if result[:airports].size == 1 
				airport = result[:airports].first
				result[:from_airport] = plane.is_from_airport(airport) ? airport : "Unknown"
				result[:to_airport]   = plane.is_to_airport(airport)   ? airport : "Unknown"
			else
				result[:from_airport] = "Unknown"
				result[:to_airport]   = "Unknown"
			end

			todays_planes << result
		end

		todays_planes
	end

	def self.planes_of_the_day2(time, lat, lon, distance, altitude)
		start_time = time.to_datetime
		end_time = time.to_datetime + 1.day

		start_time_str = start_time.in_time_zone('UTC').to_s(:db)
		end_time_str = end_time.in_time_zone('UTC').to_s(:db)

		query = <<EOS

SELECT closest_point_query.plane_track_id AS plane_track_id, 
	ST_Distance(closest_point_query.closest_point, ST_SetSRID(ST_MakePoint(#{lon}, #{lat}),4326), TRUE)/1000 AS distance,
	ST_Z(closest_point_query.closest_point) AS altitude,
	ST_M(closest_point_query.closest_point) AS time,
	ST_M(closest_point_query.closest_point_with_speed) AS speed,
	ST_AsText(closest_point_query.closest_point) AS closest_point

FROM (
	-- Find closest interpolated point on the track
	SELECT create_line_query.plane_track_id, 
            (ST_LineInterpolatePoint(
				create_line_query.line_with_time, 
				ST_LineLocatePoint(create_line_query.line_with_time, ST_SetSRID(ST_MakePoint(#{lon}, #{lat}),4326))
			)) AS closest_point,
            (ST_LineInterpolatePoint(
				create_line_query.line_with_speed, 
				ST_LineLocatePoint(create_line_query.line_with_time, ST_SetSRID(ST_MakePoint(#{lon}, #{lat}),4326))
			)) AS closest_point_with_speed
	FROM (
		-- Convert points of a track into a line string, one that include time as M nd another one that include speed as M
		SELECT ptp.plane_track_id, 
                (ST_MakeLine(
					ST_SetSRID(
    					ST_MakePoint(
        					ST_X(ST_AsText(ptp.coordinate)),
    					    ST_Y(ST_AsText(ptp.coordinate)),
    					    ST_Z(ST_AsText(ptp.coordinate)), 
    					    EXTRACT(EPOCH FROM time) 
                        ),4326
    				) ORDER BY time
    			)) AS line_with_time,
                (ST_MakeLine(
					ST_SetSRID(
    					ST_MakePoint(
    					    ST_X(ST_AsText(ptp.coordinate)),
    					    ST_Y(ST_AsText(ptp.coordinate)),
    					    ST_Z(ST_AsText(ptp.coordinate)), 
                            ptp.speed
                        ),4326
    				) ORDER BY time
    			)) AS line_with_speed
			FROM plane_track_points AS ptp INNER JOIN plane_tracks AS pt ON (ptp.plane_track_id = pt.id)
			WHERE pt.created_at < '#{end_time_str}' AND pt.last_pos_time >= '#{start_time_str}'
			GROUP BY ptp.plane_track_id
		) AS create_line_query
	) AS closest_point_query
WHERE ST_Z(closest_point_query.closest_point) < #{altitude} AND ST_Z(closest_point_query.closest_point) > 500
		 AND ST_Distance(closest_point_query.closest_point, ST_SetSRID(ST_MakePoint(#{lon}, #{lat}),4326), TRUE)/1000 < #{distance}
ORDER by time

EOS
		
		sql_result = ActiveRecord::Base.connection.execute(query)
		return [] unless sql_result.values.size > 0

		sql_data = sql_result.values

		todays_planes = []

		sql_data.each do |sql_info|
			plane_track_id, distance, altitude, time_epoch, speed, point = sql_info

			plane = PlaneTrack.find(plane_track_id)

			result = {}

			result[:plane] = plane
			result[:dist] = distance
			result[:alt] = altitude.to_i
			result[:time] = Time.at(time_epoch)
			result[:speed] = speed.to_i

			if plane.airports == nil || plane.from_airport == nil || plane.to_airport == nil

				result[:airports] = plane.was_at_airports(AIRPORTS)
	
				if result[:airports].size == 1 
					airport = result[:airports].first
					result[:from_airport] = plane.is_from_airport(airport) ? airport : "Unknown"
					result[:to_airport]   = plane.is_to_airport(airport)   ? airport : "Unknown"
				else
					result[:from_airport] = "Unknown"
					result[:to_airport]   = "Unknown"
				end

				if 10.minutes.ago > plane.last_pos_time
					plane.airports 		= result[:airports].join(",") rescue ""
					plane.from_airport	= result[:from_airport]
					plane.to_airport	= result[:to_airport]
					plane.save!
				end
			else
				result[:airports] 		= plane.airports.split(/,/)
				result[:from_airport] 	= plane.from_airport
				result[:to_airport] 	= plane.to_airport
			end

			next if result[:airports].include?("NUQ")

			# Come up with a reasonable flight number. 
			result[:flight] = plane.flight
			if plane.operator_icao != nil && (plane.flight == nil || plane.flight == plane.registration)
				result[:flight] = plane.operator_icao + "xxxx"
			elsif plane.flight == nil 
				result[:flight] = plane.registration
			end

			# When no airline is known, use operator name
			if plane.operator_icao != nil
				result[:airline] = IcaoAirline.find_by_icao_code(plane.operator_icao).airline rescue plane.operator
			else
				result[:airline] = plane.operator
			end

			# Strip year from airplane model to make strings shorter...
			result[:plane_model] = plane.model.dup rescue ""
			result[:plane_model].gsub!(/^\d\d\d\d/ , '')

			todays_planes << result
		end


		todays_planes
	end

	def self.get_flightaware_data_for_planes(planes)
		planes.each do |plane|
			next if plane.checked_flight_aware > 0
			next if 20.minutes.ago < plane.last_pos_time

			plane.get_flightaware_data
		end
	end

	def self.planes_to_gpx(planes)
		gpx = GPX::GPXFile.new

		planes.each do |pt|
			gpx.tracks << pt.to_gpx_track
			pt.to_gpx_waypoints.each { |w| gpx.waypoints << w }
		end
		gpx
	end

	def self.test
        # Create list with planes of the day
		lat = LAT
		lon = LON

		planes = planes_of_the_day(Time.new(2016,12,25), lat, lon, 5, 10000)

		planes.each_with_index do |p, index|
			plane = p[:plane]

            puts "%4d: %6d %8s %8s %3d %5d %s" % [ index, plane.id, plane.registration, plane.flight, p[:dist], p[:coord].z, p[:airports].join(",") ]
		end

		planes
	end

	def self.test2
		lat = LAT
		lon = LON

		planes = planes_of_the_day2(Time.new(2016,12,25), lat, lon, 5, 10000)
	end

end

