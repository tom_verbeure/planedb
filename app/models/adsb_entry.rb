class AdsbEntry < ApplicationRecord

	scope :close_to, -> (lat, lon, distance_in_meters = 20000) {
  		where(%{
   			ST_DWithin(
   				ST_GeographyFromText(
      				'SRID=4326;POINT(' || adsb_entries.lon || ' ' || adsb_entries.lat || ')'
   				),
   				ST_GeographyFromText('SRID=4326;POINT(%f %f)'),
   				%d
   			)
  		} % [lon, lat, distance_in_meters])
	}
end
