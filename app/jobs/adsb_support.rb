require 'net/http'
require 'addressable/uri'
require 'yajl'



module AdsbSupport 

	LAT = 37.350304
	LON = -122.046994

	def fetch_api(url, params)
		uri = Addressable::URI.parse(url)
		uri.query_values = params

		Rails.logger.debug "fetch_api: #{ uri.to_s }"
	
		request = Net::HTTP::Get.new uri
		response = Net::HTTP.start(uri.host, uri.port, :use_ssl => (uri.scheme == 'https')) { |http| http.request request }
	
		if response.is_a?(Net::HTTPSuccess)
			response.body 
		else
			nil
		end
	end
	module_function :fetch_api
	
	def fetch_api_json(url, params)
		json_txt = fetch_api(url, params)
	
		if json_txt
			parser = Yajl::Parser.new
			json = parser.parse(json_txt)
		end
	end
	module_function :fetch_api_json

	def get_adsb(lat, lon, lastDv)
		# http://www.virtualradarserver.co.uk/Documentation/Formats/AircraftList.aspx
		
		url ='https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json'
		params = {
			:lat			=> LAT,
			:lng			=> LON,
			:fDstL			=> 0,			# Lower distance
			:fDstU			=> 100,			# Upper distance
			:refreshTrail 	=> 0
		}
		if lastDv
			params[:ldv] = lastDv
		end

		json = fetch_api_json(url, params)
	end
	module_function :get_adsb

	def poll
		status = AdsbStatus.first || AdsbStatus.create
		lastDv = status.last_dv

		json = nil
		# API fetch
		Rails.logger.debug "lastDv: #{lastDv}"
		json = get_adsb(LAT, LON, lastDv)

		# Process json
		PlaneTrack.process_adsb_json(json)

		lastDv = json["lastDv"]
		status.last_dv = lastDv
		status.save!
	end
	module_function :poll

end
