class BackgroundJobJob < ApplicationJob
  	queue_as :default

  	def perform(*args)
		i = 0
		10.times do |i|
			logger.debug "Background: #{i}..."
			sleep 5
		end
  	end
end
