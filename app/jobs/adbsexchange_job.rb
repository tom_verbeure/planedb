#! /usr/bin/env ruby

require 'socket'
require 'net/telnet'
require 'net/http'
require 'yajl'
require 'addressable/uri'
require 'pp'

GOOGLE_MAPS_GEOCODING_API_KEY = 'AIzaSyCjQ-EDr_UAVxs_UqRrAyg1STGCr1E_lsM'

LAT = 37.350304
LON = -122.046994

if nil
	hostname = "pub-vrs.adsbexchange.com"
	port = 32030		# Every 30 seconds
	
	def object_parsed(obj)
		STDERR.puts "*"
		pp obj
	end
	
	def connection_completed
	end
	
	
	@parser = Yajl::Parser.new(:symbolize_keys => true)
	@parser.on_parse_complete = method(:object_parsed)
	
	if 1
		s = Net::Telnet::new("Host" => hostname,
							 "Port" => port,
							 "Telnetmode" => false,
						 	"Timeout" => 20)
	
		s.cmd(""){ |c| @parser << c }
		s.close
	end
end

def fetch_api(url, params)
	uri = Addressable::URI.parse(url)
	uri.query_values = params

	request = Net::HTTP::Get.new uri
	response = Net::HTTP.start(uri.host, uri.port, :use_ssl => (uri.scheme == 'https')) { |http| http.request request }

	if response.is_a?(Net::HTTPSuccess)
		response.body 
	else
		nil
	end
end

def fetch_api_json(url, params)
	json_txt = fetch_api(url, params)

	if json_txt
		parser = Yajl::Parser.new
		json = parser.parse(json_txt)
	end
end

def get_geocode(address)
	# https://developers.google.com/maps/documentation/geocoding/start

	url = "https://maps.googleapis.com/maps/api/geocode/json"
	params = {
		:address 	=> address,
		:key 		=> GOOGLE_MAPS_GEOCODING_API_KEY
	}
	
	json = fetch_api_json(url, params)
end

def get_latlon(address)
	geocode = get_geocode("813 Blanchard Way, Sunnyvale, CA 94087")

	if geocode
		location = geocode["results"].first["geometry"]["location"]
		lat = location["lat"]
		lon = location["lng"]
	end
	puts lat, lon

	[ lat, lon ]
end

def get_adsb(lat, lon, lastDv)
	# http://www.virtualradarserver.co.uk/Documentation/Formats/AircraftList.aspx
	
	url ='https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json'
	params = {
		:lat		=> LAT,
		:lng		=> LON,
		:fDstL		=> 0,			# Lower distance
		:fDstU		=> 50,			# Upper distance
	}
	if lastDv
		params[:ldv] = lastDv
	end

	json = fetch_api_json(url, params)
end

if 1
	lastDv = nil
	loop do
		json = get_adsb(LAT, LON, lastDv)
		puts "==================================="
		pp json
		sleep 10
	end
end




