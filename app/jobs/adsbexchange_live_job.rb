require 'net/http'
require 'addressable/uri'
require 'yajl'


LAT = 37.350304
LON = -122.046994

class AdsbexchangeLiveJob < ApplicationJob
  	queue_as :default

	def fetch_api(url, params)
		uri = Addressable::URI.parse(url)
		uri.query_values = params

		logger.debug "fetch_api: #{ uri.to_s }"
	
		request = Net::HTTP::Get.new uri
		response = Net::HTTP.start(uri.host, uri.port, :use_ssl => (uri.scheme == 'https')) { |http| http.request request }
	
		if response.is_a?(Net::HTTPSuccess)
			response.body 
		else
			nil
		end
	end
	
	def fetch_api_json(url, params)
		json_txt = fetch_api(url, params)
	
		if json_txt
			parser = Yajl::Parser.new
			json = parser.parse(json_txt)
		end
	end

	def get_adsb(lat, lon, lastDv)
		# http://www.virtualradarserver.co.uk/Documentation/Formats/AircraftList.aspx
		
		url ='https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json'
		params = {
			:lat			=> LAT,
			:lng			=> LON,
			:fDstL			=> 0,			# Lower distance
			:fDstU			=> 50,			# Upper distance
			:refreshTrail 	=> 0
		}
		if lastDv
			params[:ldv] = lastDv
		end

		json = fetch_api_json(url, params)
	end

	def poll
		status = AdsbStatus.first || AdsbStatus.create
		lastDv = status.last_dv

		json = nil
		# API fetch
		logger.debug "lastDv: #{lastDv}"
		json = get_adsb(LAT, LON, lastDv)

		# Process json
		PlaneTrack.process_adsb_json(json)

		lastDv = json["lastDv"]
		status.last_dv = lastDv
		status.save!
	end

  	def perform(*args)
		logger.debug "Starting PlaneProcess poll"
		poll
  	end
end
