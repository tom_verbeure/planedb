class PlaneTracksController < ApplicationController

	def all
		respond_to do |format|
			format.gpx do 
				send_data PlaneTrack.planes_to_gpx(PlaneTrack.all.includes(:plane_track_points)), filename: ('all_planes.gpx')
			end
		end
	end

	def show
		plane = PlaneTrack.find(params[:id])

		respond_to do |format|
			format.json
			format.gpx do 
				send_data plane.to_gpx, filename: (params[:id].to_s + '.gpx')
			end
		end
	end

	def search
		planes = []

		if params[:id]
			planes = [ PlaneTrack.find(params[:id]) ]
		elsif params[:year] && params[:month] && params[:day]
			time = Time.new(params[:year],params[:month],params[:day])

			lat 		= (params[:lat].to_f if params[:lat]) || 37.350304
			lon 		= (params[:lon].to_f if params[:lon]) || -122.046994 
			distance 	= (params[:distance].to_f if params[:distance]) || 5
			altitude 	= (params[:alt].to_f if params[:alt]) || 10000

			planes = PlaneTrack.planes_of_the_day2(time, lat, lon, distance, altitude).collect{ |result| result[:plane]}
		end


		respond_to do |format|
			format.gpx do 
				send_data PlaneTrack.planes_to_gpx(planes), filename: ('result.gpx')
			end
		end
	end

end
