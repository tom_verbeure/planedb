class PlaneListController < ApplicationController

	def today
		now = Time.now

		year = now.year
		month = now.month
		day = now.day

		common(year, month, day)
	end

	def show
		year = params[:year]
		month = params[:month]
		day = params[:day]

		common(year, month, day)
	end

	def common(year, month, day)
		lat = 37.350304
		lon = -122.046994

		date = Time.new(year, month, day)
		@todays_planes = PlaneTrack.planes_of_the_day2(date, lat, lon, 3, 9000).sort{ |x,y| x[:time] <=> y[:time] }

		PlaneTrack.get_flightaware_data_for_planes(@todays_planes.collect{ |tp| tp[:plane] })

        prev_day = date - 1.day

		render :show, :locals => { :current_day => date,
                                   :prev_day => date-1.day,
                                   :next_day => date+1.day
                                 }
	end

end
