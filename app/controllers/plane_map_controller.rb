class PlaneMapController < ApplicationController
  	def index
		@center_lat = 37.350304
		@center_lon = -122.046994

		if params[:id]
			url = "/plane_tracks/#{ params[:id] }.gpx"
		else
			url = "/plane_tracks/#{ params[:year] }/#{params[:month] }/#{params[:day] }.gpx"
		end

		common(url)
  	end

	def common(gpx_url)
		@center_lat = 37.350304
		@center_lon = -122.046994

		@gpx_url = gpx_url
		render :index
	end

end
