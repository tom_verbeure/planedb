
rails generate model adsb_entry \
	hex:string \
	"lat:decimal{9,6}" \
	"lon:decimal{9,6}" \
	altitude:integer \
	track:integer \
	speed:integer \
	messages: integer \
	seen: float \
	rssi: float \
	vert_rate: integer \
	--fixture false

