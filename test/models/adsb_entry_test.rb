require 'test_helper'

class AdsbEntryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  	test "add adsb entry" do
    	# {"hex":"a1d105","squawk":"3234","flight":"CPZ6047 ","lat":37.406818,"lon":-121.917194,"nucp":7,"seen_pos":7.3,"altitude":3500,"vert_rate":3200,"track":107,"speed":227,"category":"A3","mlat":[],"tisb":[],"messages":68,"seen":0.8,"rssi":-25.3},

		plane_0 = AdsbEntry.create!(
			hex: 		"a1d105",
			lat:		37.406818,
			lon: 		-121.917194,
			altitude: 	3500
		)

		# {"hex":"a6d3b3","type":"tisb_icao","lat":37.618119,"lon":-122.367992,"nucp":8,"seen_pos":25.4,"altitude":-150,"vert_rate":-64,"track":298,"speed":115,"mlat":[],"tisb":["lat","lon","altitude","track","speed","vert_rate"],"messages":4,"seen":25.4,"rssi":-29.7},
		plane_0 = AdsbEntry.create!(
			hex: 		"a6d3b3",
			lat:		37.618119,
			lon: 		-122.367992,
			altitude: 	-150
		)

		nearby_planes = AdsbEntry.close_to(37.5, -122.1).load

		assert_equal 1, nearby_planes.size
	end
end
