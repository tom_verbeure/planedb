
-- SELECT (pts.plane_track_id, ST_AsText(pts.coordinate)) from plane_track_points as pts group by pts.plane_track_id;



/* Convert track_points into linestring for a plane_track */
/*
UPDATE plane_tracks
SET track = subquery.line
FROM (
    SELECT pts.plane_track_id, ST_AsText(ST_MakeLine( ST_MakePoint(ST_X(ST_AsText(pts.coordinate)),ST_Y(ST_AsText(pts.coordinate)),ST_Z(ST_AsText(pts.coordinate)), 0.0) ORDER BY time)) as line
		FROM plane_track_points as pts
		WHERE pts.plane_track_id = 1
		GROUP BY pts.plane_track_id
	) AS subquery
WHERE plane_tracks.id = 1;
*/

/*
SELECT pts.plane_track_id, ST_MakeLine(ST_GeomFromText(ST_AsText(pts.coordinate)) ORDER BY time) as line, ST_AsText(ST_MakeLine(ST_GeomFromText(ST_AsText(pts.coordinate)) ORDER BY time)) as line_txt
	FROM plane_track_points as pts
	WHERE pts.plane_track_id = 1
	GROUP BY pts.plane_track_id;
*/

/*
SELECT pts.plane_track_id, ST_MakeLine(ST_GeomFromText(ST_AsText(pts.coordinate)) ORDER BY time) as line, ST_AsText(ST_MakeLine(ST_GeomFromText(ST_AsText(pts.coordinate)) ORDER BY time)) as line_txt
	FROM plane_track_points as pts
	WHERE pts.plane_track_id = 1
	GROUP BY pts.plane_track_id;
*/

/* Find all tracks that come closest to our home */
/*
SELECT subquery.plane_track_id, ST_Distance(subquery.line, ST_SetSRID(ST_MakePoint(-122.046994, 37.350304),4326), TRUE)/1000 as distance
FROM (
	-- Convert points of a track into a line string
	SELECT pts.plane_track_id, (ST_MakeLine( ST_SetSRID(ST_MakePoint(ST_X(ST_AsText(pts.coordinate)),ST_Y(ST_AsText(pts.coordinate)),ST_Z(ST_AsText(pts.coordinate)), 0.0),4326) ORDER BY time)) as line
		FROM plane_track_points as pts
		GROUP BY pts.plane_track_id
	) AS subquery
ORDER BY distance
	;
*/

SELECT closest_point_query.plane_track_id AS plane_track_id, 
	ST_Distance(closest_point_query.closest_point, ST_SetSRID(ST_MakePoint(-122.046994, 37.350304),4326), TRUE)/1000 AS distance,
	ST_Z(closest_point_query.closest_point) AS altitude,
	ST_M(closest_point_query.closest_point) AS time,
	ST_AsText(closest_point_query.closest_point) AS closest_point

FROM (
	-- Find closest interpolated point on the track
	SELECT create_line_query.plane_track_id, (ST_LineInterpolatePoint(
													create_line_query.line, 
													ST_LineLocatePoint(create_line_query.line, ST_SetSRID(ST_MakePoint(-122.046994, 37.350304),4326))
												)) AS closest_point
	FROM (
		-- Convert points of a track into a line string
		SELECT ptp.plane_track_id, (ST_MakeLine(
			   							ST_SetSRID(
											ST_MakePoint(
												ST_X(ST_AsText(ptp.coordinate)),
												ST_Y(ST_AsText(ptp.coordinate)),
												ST_Z(ST_AsText(ptp.coordinate)), 
												EXTRACT(EPOCH FROM time) ),4326
										) ORDER BY time
									)) AS line
			FROM plane_track_points AS ptp INNER JOIN plane_tracks AS pt ON (ptp.plane_track_id = pt.id)
			WHERE pt.created_at >= '2016-12-29 08:00:00' AND pt.updated_at <= '2016-12-30 00:00:00'
			GROUP BY ptp.plane_track_id
		) AS create_line_query
	) AS closest_point_query 
WHERE ST_Z(closest_point_query.closest_point) < 6000 AND ST_Z(closest_point_query.closest_point) > 500
ORDER by distance
;
