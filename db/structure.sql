--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adsb_entries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE adsb_entries (
    id integer NOT NULL,
    hex character varying,
    lat numeric(9,6),
    lon numeric(9,6),
    altitude integer,
    track integer,
    speed integer,
    messages character varying,
    "integer" character varying,
    seen character varying,
    "float" character varying,
    rssi character varying,
    vert_rate character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: adsb_entries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adsb_entries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adsb_entries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE adsb_entries_id_seq OWNED BY adsb_entries.id;


--
-- Name: adsb_statuses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE adsb_statuses (
    id integer NOT NULL,
    last_dv character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: adsb_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adsb_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adsb_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE adsb_statuses_id_seq OWNED BY adsb_statuses.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying,
    queue character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE delayed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE delayed_jobs_id_seq OWNED BY delayed_jobs.id;


--
-- Name: icao_airlines; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE icao_airlines (
    id integer NOT NULL,
    icao_code character varying(3),
    airline character varying,
    call_sign character varying,
    country character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: icao_airlines_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE icao_airlines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: icao_airlines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE icao_airlines_id_seq OWNED BY icao_airlines.id;


--
-- Name: plane_track_airports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE plane_track_airports (
    id integer NOT NULL,
    plane_track_id integer,
    airport character varying,
    movement character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: plane_track_airports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE plane_track_airports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plane_track_airports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE plane_track_airports_id_seq OWNED BY plane_track_airports.id;


--
-- Name: plane_track_points; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE plane_track_points (
    id integer NOT NULL,
    plane_track_id integer,
    "time" timestamp without time zone,
    coordinate geography(PointZ,4326),
    speed integer
);


--
-- Name: plane_track_points_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE plane_track_points_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plane_track_points_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE plane_track_points_id_seq OWNED BY plane_track_points.id;


--
-- Name: plane_tracks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE plane_tracks (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    vrs_id integer,
    icao character varying,
    flight character varying,
    registration character varying,
    airports character varying,
    last_pos_time timestamp without time zone,
    track geometry(LineStringZM),
    model character varying,
    operator character varying,
    plane_type character varying,
    operator_icao character varying,
    manufacturer character varying,
    from_airport character varying,
    to_airport character varying,
    checked_flight_aware integer DEFAULT 0
);


--
-- Name: plane_tracks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE plane_tracks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plane_tracks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE plane_tracks_id_seq OWNED BY plane_tracks.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adsb_entries ALTER COLUMN id SET DEFAULT nextval('adsb_entries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adsb_statuses ALTER COLUMN id SET DEFAULT nextval('adsb_statuses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY delayed_jobs ALTER COLUMN id SET DEFAULT nextval('delayed_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY icao_airlines ALTER COLUMN id SET DEFAULT nextval('icao_airlines_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY plane_track_airports ALTER COLUMN id SET DEFAULT nextval('plane_track_airports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY plane_track_points ALTER COLUMN id SET DEFAULT nextval('plane_track_points_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY plane_tracks ALTER COLUMN id SET DEFAULT nextval('plane_tracks_id_seq'::regclass);


--
-- Name: adsb_entries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY adsb_entries
    ADD CONSTRAINT adsb_entries_pkey PRIMARY KEY (id);


--
-- Name: adsb_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY adsb_statuses
    ADD CONSTRAINT adsb_statuses_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: icao_airlines_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY icao_airlines
    ADD CONSTRAINT icao_airlines_pkey PRIMARY KEY (id);


--
-- Name: plane_track_airports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY plane_track_airports
    ADD CONSTRAINT plane_track_airports_pkey PRIMARY KEY (id);


--
-- Name: plane_track_points_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY plane_track_points
    ADD CONSTRAINT plane_track_points_pkey PRIMARY KEY (id);


--
-- Name: plane_tracks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY plane_tracks
    ADD CONSTRAINT plane_tracks_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX delayed_jobs_priority ON delayed_jobs USING btree (priority, run_at);


--
-- Name: index_icao_airlines_on_icao_code; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_icao_airlines_on_icao_code ON icao_airlines USING btree (icao_code);


--
-- Name: index_on_adsb_entries_location; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_on_adsb_entries_location ON adsb_entries USING gist (st_geographyfromtext((((('SRID=4326;POINT('::text || lon) || ' '::text) || lat) || ')'::text)));


--
-- Name: index_plane_track_points_on_plane_track_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_plane_track_points_on_plane_track_id ON plane_track_points USING btree (plane_track_id);


--
-- Name: index_plane_tracks_on_vrs_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_plane_tracks_on_vrs_id ON plane_tracks USING btree (vrs_id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20161211035613'), ('20161211040549'), ('20161211043834'), ('20161213040440'), ('20161213042205'), ('20161213042357'), ('20161213043417'), ('20161213060723'), ('20161214054802'), ('20161214055926'), ('20161214061622'), ('20161215055507'), ('20161215060327'), ('20161219004617'), ('20161219010934'), ('20161219011433'), ('20161226061940'), ('20161226062331'), ('20161226074218'), ('20161227014317'), ('20161227014609'), ('20161227060431'), ('20161229042622'), ('20170103031123'), ('20170103034351'), ('20170103035110'), ('20170103043203'), ('20170103060913'), ('20170103064232'), ('20170105062256');


