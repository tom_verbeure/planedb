class AddPosTimeToPlaneTrack < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :last_pos_time, :datetime
  	end
end
