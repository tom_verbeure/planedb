class AddAttributesToPlaneTrack < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :vrs_id, 			:integer
		add_column :plane_tracks, :icao, 			:string
		add_column :plane_tracks, :flight, 			:string
		add_column :plane_tracks, :registration, 	:string
		add_column :plane_tracks, :airports, 		:string
  	end
end
