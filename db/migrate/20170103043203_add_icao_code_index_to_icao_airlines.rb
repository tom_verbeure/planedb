class AddIcaoCodeIndexToIcaoAirlines < ActiveRecord::Migration[5.0]
  	def up
		execute %{
			create index index_icao_airlines_on_icao_code on icao_airlines(icao_code);
		}
  	end

	def down
		execute %{
			drop index index_icao_airlines_on_icao_code;
		}
	end
end
