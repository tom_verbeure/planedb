class IndexPlaneTrackPointsOnPlaneTrackId < ActiveRecord::Migration[5.0]
  	def up
		execute %{
			create index index_plane_track_points_on_plane_track_id on plane_track_points(plane_track_id);
		}
  	end

	def down
		execute %{drop index index_plane_track_points_on_plane_track_id}
	end
end
