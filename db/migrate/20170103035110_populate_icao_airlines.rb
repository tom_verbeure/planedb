class PopulateIcaoAirlines < ActiveRecord::Migration[5.0]
  	def up
		ActiveRecord::Base.transaction do 

			line_nr = 0
			CSV.foreach("docs/IcaoAirlines.csv", :col_sep => ",", :row_sep => :auto) do |row|
				if line_nr > 0
					iata, icao_code, airline, call_sign, country, comments = row

					airline = IcaoAirline.create!(icao_code: icao_code, airline: airline, call_sign: call_sign, country: country)
				end

				line_nr += 1
			end
		end
  	end

	def down
		IcaoAirline.delete_all
	end
end

