class RenamePlaneTrackColumns < ActiveRecord::Migration[5.0]
  	def change
		remove_column :plane_tracks, :track
		rename_column :plane_tracks, :new_track, :track
  	end
end
