class AddTypeToPlaneTracks < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :plane_type, :string
  	end
end
