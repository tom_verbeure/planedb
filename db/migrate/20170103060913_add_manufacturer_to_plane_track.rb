class AddManufacturerToPlaneTrack < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :manufacturer, :string
  	end
end
