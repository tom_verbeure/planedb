class CreateNewPlaneTracks < ActiveRecord::Migration[5.0]
  	def change
    	create_table :plane_tracks do |t|

      		t.line_string :track
      		t.timestamps
    	end
  	end
end
