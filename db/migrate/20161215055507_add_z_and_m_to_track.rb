class AddZAndMToTrack < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :new_track, :line_string, :geographic => true, has_z:true, has_m:true
  	end
end
