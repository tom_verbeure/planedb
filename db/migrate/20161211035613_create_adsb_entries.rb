class CreateAdsbEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :adsb_entries do |t|
      t.string :hex
      t.decimal :lat, precision: 9, scale: 6
      t.decimal :lon, precision: 9, scale: 6
      t.integer :altitude
      t.integer :track
      t.integer :speed
      t.string :messages
      t.string :integer
      t.string :seen
      t.string :float
      t.string :rssi
      t.string :float
      t.string :vert_rate
      t.string :integer

      t.timestamps
    end
  end
end
