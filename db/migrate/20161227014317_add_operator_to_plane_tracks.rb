class AddOperatorToPlaneTracks < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :operator, :string
  	end
end
