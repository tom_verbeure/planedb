class CreateIcaoAirlines < ActiveRecord::Migration[5.0]
  	def change
    	create_table :icao_airlines do |t|
			t.column :icao_code, 		:string, limit: 3
			t.column :airline,			:string
			t.column :call_sign,		:string
			t.column :country,			:string

      		t.timestamps
    	end
  	end
end
