class CreatePlaneTrackAirports < ActiveRecord::Migration[5.0]
  def change
    create_table :plane_track_airports do |t|
      t.integer :plane_track_id
      t.string :airport
      t.string :movement

      t.timestamps
    end
  end
end
