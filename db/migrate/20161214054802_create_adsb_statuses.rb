class CreateAdsbStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :adsb_statuses do |t|
      t.integer :last_dv

      t.timestamps
    end
  end
end
