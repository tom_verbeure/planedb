class ChangeAdsbStatusLastDv < ActiveRecord::Migration[5.0]
  	def change
		change_column :adsb_statuses, :last_dv, :string
  	end
end
