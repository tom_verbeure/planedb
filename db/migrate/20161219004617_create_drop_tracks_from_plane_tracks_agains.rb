class CreateDropTracksFromPlaneTracksAgains < ActiveRecord::Migration[5.0]
  	def change
		remove_column :plane_tracks, :track
		add_column :plane_tracks, :track, :line_string, :geographics => false, has_z:true, has_m:true
    end
end
