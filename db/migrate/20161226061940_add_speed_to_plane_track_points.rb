class AddSpeedToPlaneTrackPoints < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_track_points, :speed, :integer
  	end
end
