class AddOpIcaoToPlaneTrack < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :operator_icao, :string
  	end
end
