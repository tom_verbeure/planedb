class CreatePlaneTrackPoints < ActiveRecord::Migration[5.0]
  def change
    create_table :plane_track_points do |t|
      t.integer 	:plane_track_id
	  t.point 		:coordinate, :geographics => false, has_z:true
	  t.datetime 	:time
    end
  end
end
