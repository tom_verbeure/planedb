class AddModelToPlaneTracks < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :model, :string
  	end
end
