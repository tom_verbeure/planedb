class AddCheckedFlightAwareToPlaneTrack < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :checked_flight_aware, :integer, :default => 0
  	end
end
