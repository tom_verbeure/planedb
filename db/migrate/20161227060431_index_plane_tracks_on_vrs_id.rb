class IndexPlaneTracksOnVrsId < ActiveRecord::Migration[5.0]
  	def up
		execute %{
			create index index_plane_tracks_on_vrs_id on plane_tracks(vrs_id);
		}
  	end

	def down
		execute %{drop index index_plane_tracks_on_vrs_id}
	end
end
