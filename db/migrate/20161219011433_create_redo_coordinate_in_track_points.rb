class CreateRedoCoordinateInTrackPoints < ActiveRecord::Migration[5.0]
  	def change
		remove_column :plane_track_points, :coordinate
		add_column 	  :plane_track_points, :coordinate, :st_point, geographic: true, has_z: true
  	end
end
