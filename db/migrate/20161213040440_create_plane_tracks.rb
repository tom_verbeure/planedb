class CreatePlaneTracks < ActiveRecord::Migration[5.0]
  def change
    create_table :plane_tracks do |t|
      t.line_string :track, srid: 4326, geographics: true, has_z: true, has_m: true
      t.timestamps

	  t.index :line_string, using :gist
    end
  end
end
