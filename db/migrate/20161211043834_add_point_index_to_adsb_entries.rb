class AddPointIndexToAdsbEntries < ActiveRecord::Migration[5.0]
  	def up
	  	execute %{
      		create index index_on_adsb_entries_location ON adsb_entries using gist (
        		ST_GeographyFromText(
          			'SRID=4326;POINT(' || adsb_entries.lon || ' ' || adsb_entries.lat || ')'
        		)
      		)
		}
  	end

	def down
		execute %{
			drop index index_on_adsb_entries_location
		}
	end
end
