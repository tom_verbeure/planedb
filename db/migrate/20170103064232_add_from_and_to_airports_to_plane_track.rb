class AddFromAndToAirportsToPlaneTrack < ActiveRecord::Migration[5.0]
  	def change
		add_column :plane_tracks, :from_airport, :string
		add_column :plane_tracks, :to_airport, :string
  	end
end
